package com.example.retrofitcoroutinedemo1

import com.squareup.moshi.JsonQualifier

@Retention(AnnotationRetention.RUNTIME)
@JsonQualifier
annotation class DateAnnotation