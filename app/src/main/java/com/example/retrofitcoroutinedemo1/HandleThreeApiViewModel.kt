package com.example.retrofitcoroutinedemo1

import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.retrofitcoroutinedemo1.api.GithubAccount
import kotlinx.coroutines.*

class HandleThreeApiViewModel : ViewModel() {
    val errorLiveData = MutableLiveData<String>()
    val showProgressBar = MutableLiveData<Int>()
    val userLiveData = MutableLiveData<GithubAccount>()
    val imageUrlLists = MutableLiveData<List<String>>()
    val repository = MainRepository()

    fun getData(username1: String, username2: String, username3: String) {
        viewModelScope.launch {
            // launch a coroutine for doing the network task
            // slowdown the process a bit to demonstrate everything in this coroutine is sequential
            // everything outside of this launch block is not blocked by this coroutine
            showProgressBar.value = View.VISIBLE

            val res1 = async(Dispatchers.IO) { repository.getData(username1) }
            val res2 = async(Dispatchers.IO) { repository.getData(username2) }
            val res3 = async(Dispatchers.IO) { repository.getData(username3) }

            handleResponse(res1.await(), res2.await(), res3.await())
            showProgressBar.value = View.GONE
        }
    }

    private fun handleResponse(
        userData: MainWebService.UserData,
        userData1: MainWebService.UserData,
        userData2: MainWebService.UserData
    ) {
        val list = mutableListOf<String>()
        list.add(userData.userData.avatarUrl)
        list.add(userData1.userData.avatarUrl)
        list.add(userData2.userData.avatarUrl)

        imageUrlLists.value = list
    }


    private fun handleResponse(
        list: ArrayList<String>,
        userData1: MainWebService.UserData,
        userData2: MainWebService.UserData
    ) {
        list.add(userData1.userData.avatarUrl)
        list.add(userData2.userData.avatarUrl)

        imageUrlLists.value = list
    }


    fun getData1(username1: String, username2: String, username3: String) {
        val list = mutableListOf<String>()
        viewModelScope.launch {
            showProgressBar.value = View.VISIBLE

            val res1 = async {
                val data = repository.getData(username1)
                if (data != null) {

                    list.add(data.userData.avatarUrl)

                    async {
                        val data = repository.getData(username2)
                        if (data != null)
                            list.add(data.userData.avatarUrl)
                    }
                }
            }
            val res3 = async(Dispatchers.IO) { repository.getData(username3) }


//            handleResponse(res1.await(),  res3.await())
            showProgressBar.value = View.GONE
        }
    }

    fun getData2(username1: String, username2: String, username3: String) {
        viewModelScope.launch {
            val res3 = async { repository.getData(username3) }
            val list = ArrayList<String>()

            val res1 = async {
                val data = repository.getData(username1)
                data.let {
                    list.add(data.userData.avatarUrl)
                    repository.getData(username1)
                }
            }

            handleResponse(list, res1.await(), res3.await())
        }
    }


    suspend fun getUserData(username: String) {
        viewModelScope.launch {
            // launch a coroutine for doing the network task
            // slowdown the process a bit to demonstrate everything in this coroutine is sequential
            // everything outside of this launch block is not blocked by this coroutine
            showProgressBar.value = View.VISIBLE

            launch(Dispatchers.Default) {

            }
            val res1 = async { getUserData(username) }

            val res = repository.getData(username)
            if (res.userData == null) {
                errorLiveData.value = res.error
            } else {
                userLiveData.value = res.userData
            }

            showProgressBar.value = View.GONE
        }
    }
}