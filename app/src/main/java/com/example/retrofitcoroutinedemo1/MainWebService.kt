package com.example.retrofitcoroutinedemo1

import com.example.retrofitcoroutinedemo1.api.GithubAccount
import com.example.retrofitcoroutinedemo1.api.GithubApi
import com.example.retrofitcoroutinedemo1.api.RestUtil
import kotlinx.coroutines.delay

class MainWebService {

    data class UserData( val userData: GithubAccount, val error: String)

    suspend fun getData(username: String): UserData {
        delay(5000)
        val githubService = RestUtil.instance.retrofit.create(GithubApi::class.java)
        val res = githubService.getGithubAccount1(username)
        var error : String = ""
        if (res == null)
            error = "error occured"
        return UserData(res, error)

        /* val response  = githubService.getGithubAccount("google").await()
           if (response.isSuccessful) {
               if (response.body() != null) {
                   tv_content.text = "${response.body()!!.login} \n ${response.body()!!.createdAt}"
               }
           } else {
               Toast.makeText(applicationContext, "Error ${response.code()}", Toast.LENGTH_SHORT).show()
           }*/
    }
}