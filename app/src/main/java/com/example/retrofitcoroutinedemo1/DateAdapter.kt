package com.example.retrofitcoroutinedemo1

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson



class DateAdapter {
    @ToJson
    fun toJson(@DateAnnotation date: String): String {
        return "hello$date"
    }

    @FromJson
    @DateAnnotation
    fun fromJson(date: String): String {
        return "from json - $date"
    }
}