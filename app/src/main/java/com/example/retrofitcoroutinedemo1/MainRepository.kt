package com.example.retrofitcoroutinedemo1

class MainRepository {

    val webService = MainWebService()

    suspend fun getData(username: String) : MainWebService.UserData {
        return webService.getData(username)
    }
}