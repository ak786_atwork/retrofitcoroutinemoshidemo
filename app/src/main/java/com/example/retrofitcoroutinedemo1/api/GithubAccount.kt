package com.example.retrofitcoroutinedemo1.api

import com.example.retrofitcoroutinedemo1.DateAnnotation
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass


@JsonClass(generateAdapter = true)
data class GithubAccount(
    val login : String,
    val id : Int,
    @Json(name = "avatar_url") var avatarUrl : String = "",
    @DateAnnotation
    @Json(name ="created_at")
    var createdAt : String = "",
    @Json(name ="updated_at") var updatedAt : String = "") {

    override fun equals(obj: Any?): Boolean {
        return login == (obj as GithubAccount).login
    }
}
