package com.example.retrofitcoroutinedemo1

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val viewmodel = ViewModelProviders.of(this).get(MainViewModel::class.java)

        viewmodel.userLiveData.observe(this, Observer {
            it.let {
                tv_content.text = "${it!!.login} \n ${it!!.createdAt}"
                Glide.with(this@MainActivity)
                    .load(it.avatarUrl)
                    .apply(RequestOptions.circleCropTransform())
                    .into(avatar)
            }
        })

        viewmodel.errorLiveData.observe(this, Observer {
            it.let { showToast(it) }
        })

        viewmodel.showProgressBar.observe(this, Observer {
            it.let { progress_circular.visibility = it }
        })

        viewmodel.getData("anil")
        showToast("The above network call in coroutine is not blocking this toast")
    }

    private fun showToast(info: String) {
        Toast.makeText(applicationContext, info, Toast.LENGTH_LONG).show()

    }

}
