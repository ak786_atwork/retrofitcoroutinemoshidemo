package com.example.retrofitcoroutinedemo1

import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.retrofitcoroutinedemo1.api.GithubAccount
import kotlinx.coroutines.launch

class MainViewModel : ViewModel() {

    val errorLiveData = MutableLiveData<String>()
    val showProgressBar = MutableLiveData<Int>()
    val userLiveData = MutableLiveData<GithubAccount>()
    val repository = MainRepository()

    fun getData(username: String) {
        viewModelScope.launch {
            // launch a coroutine for doing the network task
            // slowdown the process a bit to demonstrate everything in this coroutine is sequential
            // everything outside of this launch block is not blocked by this coroutine
            showProgressBar.value = View.VISIBLE

            val res = repository.getData(username)
            if (res.userData == null) {
                errorLiveData.value = res.error
            } else {
                userLiveData.value = res.userData
            }

            showProgressBar.value = View.GONE
        }
    }
}