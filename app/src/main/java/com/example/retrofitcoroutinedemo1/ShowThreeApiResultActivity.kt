package com.example.retrofitcoroutinedemo1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_show_three_api_result.*

class ShowThreeApiResultActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_three_api_result)

        val viewModel = ViewModelProviders.of(this).get(HandleThreeApiViewModel::class.java)

        viewModel.imageUrlLists.observe(this, Observer {
            loadImage(avatar1, it[0])
            loadImage(avatar2, it[1])
            loadImage(avatar3, it[2])
        })

        viewModel.showProgressBar.observe(this, Observer {
            it.let { progress_circular1.visibility = it }
        })


        viewModel.getData2("anil", "yunis","raj")
    }

    private fun loadImage(imageView: ImageView, url: String) {
        Glide.with(this@ShowThreeApiResultActivity)
            .load(url)
            .apply(RequestOptions.circleCropTransform())
            .into(imageView)
    }
}
